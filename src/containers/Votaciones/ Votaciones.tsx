import React, { useState } from "react";
import IEvento from "../../models/Evento";
import {
  IconButton,
  makeStyles,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/AddOutlined";
import VotacionCard from "../../components/VotacionCard";
import VotacionFormDialog from "../../components/VotacionFormDialog";
import { useFirestoreConnect, useFirestore } from "react-redux-firebase";
import { useSelector } from "react-redux";
import { VotacionesQuery } from "../../types";
import Votacion from "../../models/Votacion";

interface Props {
  evento: IEvento;
}

const useStyles = makeStyles({
  headerDiv: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
  },
  votations: {
    display: "flex",
    justifyContent: "space-around",
    flexWrap: "wrap"
  }
});

const Votaciones: React.FC<Props> = ({ evento }) => {
  const classes = useStyles();

  const [dialogOpen, setDialogOpen] = useState(false);

  const firestore = useFirestore();

  useFirestoreConnect({
    collection: "eventos",
    doc: evento.id,
    subcollections: [
      {
        collection: "votaciones"
      }
    ],

    storeAs: "votaciones"
  });

  const votations = useSelector(
    ({
      firestore: {
        ordered: { votaciones }
      }
    }: VotacionesQuery) => {
      return votaciones;
    }
  );
  const handleOpenDialog = () => {
    setDialogOpen(true);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  const handleVoteClick = async (votacionId: string, cantidad: number) => {
    try {
      await firestore.update(`eventos/${evento.id}/votaciones/${votacionId}`, {
        votos: cantidad
      });
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <div className={classes.headerDiv}>
        <h2>Votaciones</h2>

        <IconButton onClick={handleOpenDialog}>
          <AddIcon />
        </IconButton>
      </div>

      <div className={classes.votations}>
        {votations &&
          votations.map((v: Votacion, i: number) => {
            return (
              <VotacionCard
                key={i}
                votacion={v}
                evento={evento}
                quantityCallback={handleVoteClick}
              />
            );
          })}
      </div>

      {dialogOpen && (
        <VotacionFormDialog
          open={dialogOpen}
          onClose={handleDialogClose}
          evento={evento}
        />
      )}
    </div>
  );
};

export default Votaciones;
