import React, { useState } from "react";
import { IconButton } from "@material-ui/core";
import EditIcon from "@material-ui/icons/EditOutlined";
import { makeStyles } from "@material-ui/styles";
import InvitadoList from "../../components/InvitadoList";
import InvitadoAddDialog from "../../components/InvitadoAddDialog";
import IEvento from "../../models/Evento";
import { useFirestoreConnect } from "react-redux-firebase";
import { useSelector } from "react-redux";
import { InvitadosQuery } from "../../types";
import Spinner from "../../components/Spinner";

interface Props {
  evento: IEvento;
}

const useStyles = makeStyles({
  headerDiv: {
    display: "flex",
    justifyContent: "space-between"
  }
});

const Invitados: React.FC<Props> = ({ evento }) => {
  const [dialogOpen, setDialogOpen] = useState(false);

  useFirestoreConnect({
    collection: "invitaciones",
    where: ["eventoId", "==", evento.id]
  });

  const invitados = useSelector(
    ({
      firestore: {
        ordered: { invitaciones }
      }
    }: InvitadosQuery) => {
      return invitaciones;
    }
  );

  const classes = useStyles();

  const handleEditClick = () => {
    setDialogOpen(true);
  };
  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  return (
    <div>
      <div className={classes.headerDiv}>
        <h2>Invitados</h2>
        <IconButton onClick={handleEditClick}>
          <EditIcon />
        </IconButton>
      </div>

      {invitados ? <InvitadoList invitados={invitados} /> : <Spinner />}

      {dialogOpen && (
        <InvitadoAddDialog
          open={dialogOpen}
          onClose={handleDialogClose}
          evento={evento}
        />
      )}
    </div>
  );
};

export default Invitados;
