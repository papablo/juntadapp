import React, { useState } from "react";
import RequerimientoList from "../../components/RequerimientoList";
import { QuantityCallback } from "../../types";
import { IconButton } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import { makeStyles } from "@material-ui/styles";
import RequerimientoFormDialog from "../../components/RequerimientoFormDialog";
import IEvento from "../../models/Evento";
import { useFirestoreConnect, useFirestore } from "react-redux-firebase";
import { useSelector } from "react-redux";
import Spinner from "../../components/Spinner";

const useStyles = makeStyles({
  headerDiv: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: "1em",
    marginBottom: "1em"
  }
});

interface RequerimientoQuery {
  firestore: { ordered: { requerimientos: any[] } };
}
interface Props {
  evento: IEvento;
}

const Requerimientos: React.FC<Props> = ({ evento }) => {
  const classes = useStyles();

  const [dialogOpen, setDialogOpen] = useState(false);

  useFirestoreConnect({
    collection: "eventos",
    doc: evento.id,
    subcollections: [
      {
        collection: "requerimientos"
      }
    ],

    storeAs: "requerimientos"
  });

  const requerimientos = useSelector(
    ({
      firestore: {
        ordered: { requerimientos }
      }
    }: RequerimientoQuery) => {
      return requerimientos;
    }
  );

  const firestore = useFirestore();

  const quantityCallback: QuantityCallback = async (
    requerimientoId: string,
    cantidad: number
  ) => {
    try {
      firestore.update(
        `eventos/${evento.id}/requerimientos/${requerimientoId}`,
        { cantidad: cantidad }
      );
    } catch (error) {}
  };

  const handleAddRequerimiento = () => {
    setDialogOpen(true);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  return (
    <div>
      <div className={classes.headerDiv}>
        <h2>Requerimientos</h2>
        <IconButton
          color={"primary"}
          size={"medium"}
          onClick={handleAddRequerimiento}
        >
          <AddIcon />
        </IconButton>
      </div>

      {requerimientos ? (
        <RequerimientoList
          evento={evento}
          quantityCallback={quantityCallback}
          requerimientos={requerimientos}
        />
      ) : (
        <Spinner />
      )}

      {dialogOpen && (
        <RequerimientoFormDialog
          evento={evento}
          open={dialogOpen}
          onClose={handleDialogClose}
        />
      )}
    </div>
  );
};

export default Requerimientos;
