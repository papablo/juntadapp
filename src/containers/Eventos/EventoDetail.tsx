import React from "react";

import { RouteComponentProps } from "react-router";
import {
  useFirestoreConnect,
  useFirestore,
  isLoaded
} from "react-redux-firebase";
import { useSelector } from "react-redux";
import IEvento, { EventosFirebaseQueryResult } from "../../models/Evento";
import Spinner from "../../components/Spinner";
import DetailCard from "../../components/DetailCard";
import {
  convertTimestampToMoment,
  convertTimestampToString
} from "../../utils";
import { indexableEntity, AuthQuery } from "../../types";
import DetailActionsHeader from "../../components/DetailActionsHeader";
import Requerimientos from "../Requerimientos/Requerimientos";
import Invitados from "../Invitados/Invitados";
import { Card, CardContent } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Votaciones from "../Votaciones/ Votaciones";

const useStyles = makeStyles({
  card: {
    margin: "1em 0"
  }
});

interface EventoDetailProps {
  eventoId: string;
}

interface Props extends RouteComponentProps<EventoDetailProps> {}

const EventoDetail: React.FC<Props> = ({ history, match }) => {
  const {
    params: { eventoId }
  } = match;

  const classes = useStyles();

  useFirestoreConnect({
    collection: "eventos",
    doc: eventoId
  });

  const auth = useSelector(({ firebase: { auth } }: AuthQuery) => auth);

  const evento: IEvento = useSelector(
    ({
      firestore: {
        ordered: { eventos }
      }
    }: EventosFirebaseQueryResult) => {
      const [evento] = eventos ? eventos : [];

      return evento;
    }
  );

  const firestore = useFirestore();

  const deleteCallback = async () => {
    try {
      await firestore.delete(`eventos/${evento.id}`);
    } catch (error) {
      console.error(error);
    }
    history.goBack();
  };

  if (evento) {
    const formattedEvento = {
      ...evento,
      momentTimestamp: convertTimestampToMoment(evento.timestamp),
      stringTimestamp: convertTimestampToString(
        evento.timestamp,
        "YYYY-MM-DD - HH:mm"
      )
    };

    const labels = [
      { label: "Nombre del Evento", name: "nombre" },
      { label: "Fecha y Hora", name: "stringTimestamp" },
      { label: "Dirección", name: "direccion" },
      { label: "Anfitrion", name: "anfitrion" }
    ];

    return (
      <div>
        <DetailActionsHeader
          goBackHref={"/evento"}
          goBackDestination={"Eventos"}
          editHref={`/evento/${evento.id}/editar`}
          deleteCallback={deleteCallback}
          showEditDelete={isLoaded(auth) && auth.email === evento.anfitrion}
        />
        <DetailCard
          labels={labels}
          entity={formattedEvento as indexableEntity}
        />

        <Card raised className={classes.card}>
          <CardContent>
            <Requerimientos evento={evento} />
          </CardContent>
        </Card>

        <Card raised className={classes.card}>
          <CardContent>
            <Invitados evento={evento} />
          </CardContent>
        </Card>

        <Card raised className={classes.card}>
          <CardContent>
            <Votaciones evento={evento} />
          </CardContent>
        </Card>
      </div>
    );
  } else {
    return <Spinner />;
  }
};

export default EventoDetail;
