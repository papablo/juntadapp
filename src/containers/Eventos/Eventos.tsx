import React from "react";
import {
  Button,
  makeStyles,
  createStyles,
  List,
  ListItem,
  Divider
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import Evento, { EventosFirebaseQueryResult } from "../../models/Evento";
import EventoCard from "../../components/EventoCard";
import { useFirestoreConnect } from "react-redux-firebase";
import { useSelector } from "react-redux";
import Spinner from "../../components/Spinner";
import { InvitadosQuery, AuthQuery } from "../../types";

const useStyles = makeStyles(
  createStyles({
    buttonBar: {
      display: "flex",
      justifyContent: "flex-end"
    }
  })
);

const Eventos: React.FC = () => {
  const classes = useStyles();

  const email = useSelector(({ firebase: { auth } }: AuthQuery) => {
    return auth.email;
  });

  useFirestoreConnect([
    {
      collection: "eventos",
      where: ["anfitrion", "==", email ? email : ""]
    },
    {
      collection: "invitaciones",
      where: ["email", "==", email ? email : ""]
    }
  ]);

  const invitaciones = useSelector(
    ({
      firestore: {
        ordered: { invitaciones }
      }
    }: InvitadosQuery) => {
      return invitaciones;
    }
  );

  const misEventos: any[] = useSelector(
    ({
      firestore: {
        ordered: { eventos }
      }
    }: EventosFirebaseQueryResult) => {
      return eventos;
    }
  );

  return (
    <div>
      <div className={classes.buttonBar}>
        <Button
          variant="contained"
          startIcon={<AddIcon />}
          color="secondary"
          href={"/evento/crear"}
        >
          Crear Evento
        </Button>
      </div>

      {misEventos ? (
        <div>
          <h1>Eventos creados por mí</h1>

          {misEventos.map((evento: Evento, i: number) => (
            <EventoCard key={i} evento={evento} />
          ))}
        </div>
      ) : (
        <Spinner />
      )}

      {invitaciones ? (
        <div>
          <h1>Eventos a los que me han invitado</h1>

          <List>
            {invitaciones.map((invite: any, i: number) => (
              <div key={i}>
                <ListItem
                  component={"a"}
                  button
                  href={`/evento/${invite.eventoId}`}
                >
                  {invite.nombre}
                </ListItem>
                <Divider />
              </div>
            ))}
          </List>
        </div>
      ) : (
        <Spinner />
      )}
    </div>
  );
};

export default Eventos;
