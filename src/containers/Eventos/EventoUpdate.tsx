import React from "react";
import { EventosFirebaseQueryResult } from "../../models/Evento";
import EventoForm from "../../components/EventoForm";
import CreateOrUpdateForm from "../../components/CreateOrUpdateForm";
import { RouteComponentProps } from "react-router";
import { useFirestoreConnect } from "react-redux-firebase";
import IEvento from "../../models/Evento";
import { useSelector } from "react-redux";

interface EventoUpdateProps {
  eventoId: string;
}

interface Props extends RouteComponentProps<EventoUpdateProps> {}

const EventoUpdate: React.FC<Props> = ({ history, match }) => {
  const {
    params: { eventoId }
  } = match;

  useFirestoreConnect({
    collection: "eventos",
    doc: eventoId,
  });

  const evento: IEvento = useSelector(
    ({
      firestore: {
        ordered: { eventos }
      }
    }: EventosFirebaseQueryResult) => {
      const [evento] = eventos ? eventos : [];

      return evento;
    }
  );

  const isCreate = match.path.includes("crear");

  const onFormCancel = () => {
    history.goBack();
  };

  const onFormSuccess = () => {
    history.goBack();
  };

  return (
    <div>
      <CreateOrUpdateForm
        title={"Evento"}
        isCreate={isCreate}
        entity={evento}
        formComponent={EventoForm}
        onFormCancel={onFormCancel}
        onFormSuccess={onFormSuccess}
      />
    </div>
  );
};

export default EventoUpdate;
