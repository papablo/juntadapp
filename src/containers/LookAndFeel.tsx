import React from "react";
import {
  Container,
  Typography,
  createStyles,
  makeStyles,
  Theme,
  Card,
  CardContent,
  TextField
} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    mainContainer: {
      paddingTop: "1em"
    },
    textField: {
      margin: ".5em"
    }
  })
);

const LookAndFeel: React.FC = () => {
  const classes = useStyles();
  return (
    <div className={classes.mainContainer}>
      <Container>
        <Typography variant="h3">Look & Feel</Typography>

        <Typography variant="h6">
          Esta página servirá de prueba para diferentes componentes
        </Typography>

        <main>
          <h2>Listados</h2>

          <h3>Con cards</h3>

          <Card>
            <CardContent>
              <Typography component="h2" variant="h5">
                El evento
              </Typography>
              <Typography variant="body1">Una descripcion</Typography>
              <Typography variant="body2">23/10/2019 - 20:30 hs</Typography>
            </CardContent>
          </Card>

          <h2>Formulario</h2>
          <div>
            <form action="">
              <TextField
                className={classes.textField}
                variant="outlined"
                label="Nombre"
                type="text"
              />
              <TextField
                className={classes.textField}
                variant="outlined"
                label="Ubicación"
                type="text"
              />
              <TextField
                className={classes.textField}
                variant="outlined"
                label="Cantidad"
                type="number"
              />
            </form>
          </div>
        </main>
      </Container>
    </div>
  );
};

export default LookAndFeel;
