import React from "react";
import { Button } from "@material-ui/core";

const Home: React.FC = () => {
  return (
    <div>
      <h1>Juntadapp</h1>
      <Button href={'/evento'}>Eventos</Button>
    </div>
  );
};

export default Home;
