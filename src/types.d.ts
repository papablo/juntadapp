interface f {
  (): void;
}
export interface FormProps {
  entity?: any;
  onFormCancel: f;
  onFormSuccess: f;
}

export interface indexableEntity {
  [name: string]: any;
}

export interface QuantityCallback {
  (requerimientoId: string, quantity: number): void;
}

export interface VoteCallback {
  (votacionId: string, quantity: number): void;
}

export interface VotationManipulation {
  quantityCallback: VoteCallback;
}
export interface RequerimientoManipulation {
  quantityCallback: QuantityCallback;
}

export interface ProfileQuery {
  firebase: { profile: any };
}
export interface AuthQuery {
  firebase: { auth: any };
}

export interface InvitadosQuery {
  firestore: { ordered: { invitaciones: any } };
}

export interface VotacionesQuery {
  firestore: { ordered: { votaciones: any } };
}
