import moment from "moment";

const  convertTimestampToMoment = ({
  seconds
}: {
  seconds: number;
}) : moment.Moment => {
  return moment.unix(seconds);
};

const convertTimestampToString = (timestamp: { seconds: number }, format:string):string => {
  return convertTimestampToMoment(timestamp).format(format);
};

export {convertTimestampToMoment, convertTimestampToString }
