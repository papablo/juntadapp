import React from "react";
import ReactDOM from "react-dom";
import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

import { createStore, combineReducers } from "redux";
import {
  ReactReduxFirebaseProvider,
  firebaseReducer
} from "react-redux-firebase";
import { createFirestoreInstance, firestoreReducer } from "redux-firestore";

import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import firebaseConfig from "./config/fibaseConfig";
import { Provider } from "react-redux";

const rffConfig = {
  userProfile: "users",
  useFirestoreForProfile: true
};

firebase.initializeApp(firebaseConfig);

const rootReducer = combineReducers({
  firebase: firebaseReducer,
  firestore: firestoreReducer
});

const store = createStore(
  rootReducer,
  {},
  (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);

const rffProps = {
  firebase,
  config: rffConfig,
  dispatch: store.dispatch,
  createFirestoreInstance
};

const ProvidedApp = () => (
  <Provider store={store}>
    <ReactReduxFirebaseProvider {...rffProps}>
      <App />
    </ReactReduxFirebaseProvider>
  </Provider>
);
ReactDOM.render(<ProvidedApp />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
