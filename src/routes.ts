import Home from "./containers/Home";
import Eventos from "./containers/Eventos/Eventos";
import EventoUpdate from "./containers/Eventos/EventoUpdate";
import EventoDetail from "./containers/Eventos/EventoDetail";

export interface RoutePath {
  exact?: boolean;
  path: string;
  component: React.FC;
}

const eventoRoutes: RoutePath[] = [
  { path: "/evento/crear", component: EventoUpdate as any },
  { path: "/evento/:eventoId/editar", component: EventoUpdate as any },
  { path: "/evento/:eventoId", component: EventoDetail as any },
  { path: "/evento", component: Eventos }
];

export const routes: RoutePath[] = [
  ...eventoRoutes,
  { exact: true, path: "/", component: Home },
];
