export default interface IRequerimiento {
  id: string;
  articulo: string;
  cantidad: number;
  necesitado: number;
}
