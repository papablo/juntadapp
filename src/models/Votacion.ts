export default interface Votacion {
  id: string;
  asunto: string;
  votos: number;
}
