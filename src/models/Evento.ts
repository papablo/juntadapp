export default interface IEvento {
  id: string;
  nombre: string;
  direccion: string;
  anfitrion:string;
  timestamp: { seconds: number };
}

export interface EventosFirebaseQueryResult {
  firestore: { ordered: { eventos: any[] } };
}