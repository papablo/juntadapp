import React, { useState, useEffect } from "react";
import {
  Card,
  CardContent,
  Typography,
  Button,
  makeStyles,
  CardActions,
  IconButton
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import Votacion from "../models/Votacion";
import IEvento from "../models/Evento";
import { useFirestore, isLoaded } from "react-redux-firebase";
import { useSelector } from "react-redux";
import { AuthQuery, VotationManipulation } from "../types";

const useStyles = makeStyles({
  card: {
    marginTop: "1em"
  },
  votacionContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  legend: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  }
});

interface Props extends VotationManipulation {
  votacion: Votacion;
  evento: IEvento;
}
const VotacionCard: React.FC<Props> = ({
  votacion,
  evento,
  quantityCallback
}) => {
  const classes = useStyles();

  const [cantidad, setCantidad] = useState(votacion.votos);

  useEffect(() => {
    setCantidad(votacion.votos);
  });

  const auth = useSelector(({ firebase: { auth } }: AuthQuery) => {
    return auth;
  });

  const firestore = useFirestore();

  const handleDeleteClick = async () => {
    try {
      firestore.delete(`eventos/${evento.id}/votaciones/${votacion.id}`);
    } catch (error) {
      console.error(error);
    }
  };

  const handleVotarClick = async () => {
    setCantidad(prevCantidad => {
      const newCantidad = prevCantidad + 1;

      quantityCallback(votacion.id, newCantidad);

      return newCantidad;
    });
  };

  return (
    <Card className={classes.card}>
      <CardContent>
        <div className={classes.votacionContainer}>
          <div className={classes.legend}>
            <Typography>{votacion.asunto}</Typography>
            <Typography>{cantidad}</Typography>
          </div>
          <Button onClick={handleVotarClick} variant={"outlined"}>
            Votar
          </Button>
        </div>
      </CardContent>
      {isLoaded(auth) && auth.email === evento.anfitrion && (
        <CardActions>
          <IconButton onClick={handleDeleteClick}>
            <DeleteIcon />
          </IconButton>
        </CardActions>
      )}
    </Card>
  );
};

export default VotacionCard;
