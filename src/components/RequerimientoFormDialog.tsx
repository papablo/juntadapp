import React, { useState, ChangeEvent } from "react";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  DialogContentText,
  TextField,
  DialogActions,
  Button
} from "@material-ui/core";
import { useFirestore } from "react-redux-firebase";
import IEvento from "../models/Evento";

interface Props {
  open: boolean;
  onClose: { (): void };
  evento: IEvento;
}

const RequerimientoFormDialog: React.FC<Props> = ({
  open,
  onClose,
  evento
}) => {
  const [isSaving, setIsSaving] = useState(false);

  const [dialogOpen, setDialogOpen] = useState(open);
  const [articulo, setArticulo] = useState("");
  const [necesitado, setNecesitado] = useState(0);

  const handleArticuloChange = ({
    target: { value }
  }: ChangeEvent<HTMLInputElement>) => {
    setArticulo(value);
  };

  const handleNecesitadoChange = ({
    target: { value }
  }: ChangeEvent<HTMLInputElement>) => {
    setNecesitado(() => {
      let _value = parseInt(value);

      if (!_value) {
        _value = 0;
      }

      return _value;
    });
  };

  const handleClose = () => {
    setDialogOpen(false);
    onClose();
  };

  const firestore = useFirestore();

  const handleCreate = async () => {
    setIsSaving(true);
    const requerimiento = {
      articulo,
      cantidad: 0,
      necesitado
    };
    try {
      await firestore.add(`eventos/${evento.id}/requerimientos`, requerimiento);
    } catch (error) {
      console.error(error);
    }

    handleClose();
  };

  return (
    <div>
      <Dialog
        fullWidth
        open={dialogOpen}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Crear Requerimiento</DialogTitle>
        <DialogContent>
          <DialogContentText>¿Qué hace falta?</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            value={articulo}
            onChange={handleArticuloChange}
            id="requerimiento-input"
            label="Requerimiento"
            type="text"
            fullWidth
          />
          <TextField
            autoFocus
            margin="dense"
            value={necesitado}
            onChange={handleNecesitadoChange}
            id="necesitado-input"
            label="Cantidad Necesaria"
            type="number"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button disabled={isSaving} onClick={handleClose} color="primary">
            Cancelar
          </Button>
          <Button disabled={isSaving} onClick={handleCreate} color="primary">
            Crear Requerimiento
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default RequerimientoFormDialog;
