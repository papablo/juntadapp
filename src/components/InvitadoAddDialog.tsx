import React, { useState, ChangeEvent } from "react";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  DialogContentText,
  TextField,
  DialogActions,
  Button
} from "@material-ui/core";
import IEvento from "../models/Evento";
import { makeStyles } from "@material-ui/styles";
import { useFirestore } from "react-redux-firebase";

interface Props {
  open: boolean;
  onClose: { (): void };
  evento: IEvento;
}

const useStyles = makeStyles({
  inviteForm: {
    display: "flex",
    flexDirection: "column"
  },
  inviteButton: {
    marginTop: "1em"
  }
});
const InvitadoAddDialog: React.FC<Props> = ({ open, onClose, evento }) => {
  const [isInviting, setIsInviting] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(open);
  const [email, setEmail] = useState("");

  const classes = useStyles();

  const handleClose = () => {
    setDialogOpen(false);
    onClose();
  };

  const handleSearchTermChange = ({
    target: { value }
  }: ChangeEvent<HTMLInputElement>) => {
    setEmail(() => {
      return value;
    });
  };

  const firestore = useFirestore();

  const handleInvite = async () => {
    setIsInviting(true);
    try {
      await firestore.add(`invitaciones`, {
        email,
        eventoId: evento.id,
        nombre: evento.nombre
      });
      setEmail("");
      setIsInviting(false);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <Dialog
        fullWidth
        open={dialogOpen}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Crear Requerimiento</DialogTitle>
        <DialogContent>
          <DialogContentText>¿A quién invitar?</DialogContentText>
          <div className={classes.inviteForm}>
            <TextField value={email} onChange={handleSearchTermChange} />
            <Button
              disabled={isInviting}
              onClick={handleInvite}
              className={classes.inviteButton}
            >
              invitar
            </Button>
          </div>
        </DialogContent>
        <DialogActions></DialogActions>
      </Dialog>
    </div>
  );
};

export default InvitadoAddDialog;
