import React from "react";

import IRequerimiento from "../models/Requerimiento";
import RequerimientoListItem from "./RequerimientoListItem";
import { RequerimientoManipulation } from "../types";
import IEvento from "../models/Evento";

interface Props extends RequerimientoManipulation {
  requerimientos: IRequerimiento[];
  evento: IEvento;
}

const RequerimientoList: React.FC<Props> = ({
  evento,
  requerimientos,
  quantityCallback
}) => {
  return (
    <div>
      {requerimientos.map((requerimiento, i: number) => (
        <RequerimientoListItem
          key={i}
          evento={evento}
          requerimiento={requerimiento}
          quantityCallback={quantityCallback}
        />
      ))}
    </div>
  );
};

export default RequerimientoList;
