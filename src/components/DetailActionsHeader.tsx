import React, { useState } from "react";
import { makeStyles, createStyles } from "@material-ui/styles";
import { Button } from "@material-ui/core";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import ConfirmationDialog from "./ConfirmationDialog";

const useStyles = makeStyles(
  createStyles({
    buttonBar: {
      display: "flex",
      paddingBottom: ".8em",
      justifyContent: "space-between"
    },
    editButton: {
      marginRight: "1em"
    },
    actionButtons: {
      display: "flex",
      justifyContent: "flex-end",
      alignItmes: "center",
      flexWrap: "wrap"
    }
  })
);

interface Props {
  goBackHref: string;
  editHref: string;
  goBackDestination: string;
  deleteCallback: { (): void };
  showEditDelete: boolean;
}

const DetailActionsHeader: React.FC<Props> = ({
  goBackHref,
  goBackDestination,
  editHref,
  deleteCallback,
  showEditDelete
}) => {
  const [dialogOpen, setDialogOpen] = useState(false);

  const classes = useStyles();

  const handleNegativeClick = () => {
    setDialogOpen(false);
  };
  const handlePositiveClick = async () => {
    deleteCallback();
  };

  const handleClickDelete = () => {
    setDialogOpen(true);
  };

  return (
    <div className={classes.buttonBar}>
      <Button
        startIcon={<ArrowLeftIcon />}
        variant={"contained"}
        href={goBackHref}
      >
        Volver a {goBackDestination}
      </Button>
      <div className={classes.actionButtons}>
        {showEditDelete && editHref && (
          <Button
            startIcon={<EditIcon />}
            variant={"contained"}
            href={editHref}
            color={"primary"}
            className={classes.editButton}
          >
            Editar
          </Button>
        )}
        {showEditDelete && deleteCallback && (
          <Button
            startIcon={<DeleteIcon />}
            variant={"contained"}
            onClick={handleClickDelete}
            color="secondary"
          >
            Eliminar
          </Button>
        )}
        {deleteCallback && dialogOpen && (
          <ConfirmationDialog
            open={dialogOpen}
            title={"Confirmar Borrado?"}
            content={"Confirmar Borrado"}
            affirmativeAnswer={"Sí"}
            negativeAnswer={"No"}
            onClickNegative={handleNegativeClick}
            onClickPositive={handlePositiveClick}
          />
        )}
      </div>
    </div>
  );
};

export default DetailActionsHeader;
