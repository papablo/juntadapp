import React from "react";
import IEvento from "../models/Evento";
import {
  Card,
  CardContent,
  Typography,
  CardActionArea
} from "@material-ui/core";
import { makeStyles, createStyles } from "@material-ui/styles";
import moment from "moment";

const useStyles = makeStyles(
  createStyles({
    card: {
      marginTop: "1em"
    },
    bottomActions: {
      display: "flex",
      justifyContent: "space-between"
    }
  })
);

interface EventoCardProps {
  evento: IEvento;
}
const EventoCard: React.FC<EventoCardProps> = ({ evento }: EventoCardProps) => {
  const classes = useStyles();
  const seconds = evento.timestamp.seconds;

  const formattedTimestamp = moment.unix(seconds).format("YYYY-MM-DD - HH:mm");

  return (
    <div>
      <Card raised className={classes.card}>
        <CardActionArea href={`/evento/${evento.id}`}>
          <CardContent>
            <Typography component="h2" variant="h5">
              {evento.nombre}
            </Typography>
            <Typography component="h5">
              Organizado por: {evento.anfitrion}
            </Typography>
            <Typography variant="body1">Lugar: {evento.direccion}</Typography>
            <Typography variant="body2">{formattedTimestamp}</Typography>
          </CardContent>
        </CardActionArea>
      </Card>
    </div>
  );
};

export default EventoCard;
