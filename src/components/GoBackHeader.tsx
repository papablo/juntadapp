import React from "react";
import { makeStyles, createStyles } from "@material-ui/styles";
import { IconButton, Button } from "@material-ui/core";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";

const useStyles = makeStyles(
  createStyles({
    buttonBar: {
      display: "flex",
      paddingBottom: ".8em"
    }
  })
);

interface Props {
  goBackHref: string;
  goBackDestination: string;
}

const GoBackHeader: React.FC<Props> = ({ goBackHref, goBackDestination }) => {
  const classes = useStyles();
  return (
    <div className={classes.buttonBar}>
      <Button startIcon={<ArrowLeftIcon />} variant={"contained"} href={goBackHref}>
        Volver a {goBackDestination}
      </Button>
    </div>
  );
};

export default GoBackHeader;
