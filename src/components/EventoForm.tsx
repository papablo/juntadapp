import React, { useState, FormEvent, ChangeEvent } from "react";
import Evento from "../models/Evento";
import { Button, TextField } from "@material-ui/core";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { FormProps, AuthQuery } from "../types";
import { makeStyles, createStyles } from "@material-ui/styles";
import moment from "@date-io/moment";

import "moment/locale/es-us";
import { useFirestore, isLoaded } from "react-redux-firebase";
import { Moment } from "moment";
import { convertTimestampToMoment } from "../utils";
import { useSelector } from "react-redux";

interface Props extends FormProps {
  entity?: Evento;
}

const useStyles = makeStyles(
  createStyles({
    formDiv: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "space-around"
    },
    buttonBar: {
      marginTop: "1em",
      display: "flex",
      flexDirection: "row"
    },
    cancelButton: {
      marginLeft: "1em"
    },
    field: {
      marginBottom: ".5em"
    }
  })
);

const EventoForm: React.FC<Props> = ({
  entity,
  onFormCancel,
  onFormSuccess
}: Props) => {
  const classes = useStyles();

  const [isSaving, setIsSaving] = useState(false);

  const [timestamp, setTimestamp] = useState(
    entity ? convertTimestampToMoment(entity.timestamp).toDate() : new Date()
  );
  const [nombre, setNombre] = useState(entity ? entity.nombre : "");
  const [direccion, setDireccion] = useState(entity ? entity.direccion : "");

  const handleNombreChange = ({
    target: { value }
  }: ChangeEvent<HTMLInputElement>) => {
    setNombre(value);
  };

  const handleDireccionChange = ({
    target: { value }
  }: ChangeEvent<HTMLInputElement>) => {
    setDireccion(value);
  };

  const handleDateChange = (e: any) => {
    setTimestamp((e as Moment).toDate());
  };

  const auth = useSelector(({ firebase: { auth } }: AuthQuery) => {
    return auth;
  });

  const firestore = useFirestore();

  const buttonText = entity ? "Actualizar" : "Crear";

  const onFormSubmit = async (e: FormEvent) => {
    e.preventDefault();
    setIsSaving(true);

    const { email } = auth;
    const anfitrion = `${email}`;

    const evento = {
      timestamp: timestamp as Date,
      nombre,
      direccion,
      anfitrion
    };

    try {
      if (entity) {
        await firestore.update(`eventos/${entity.id}`, evento);
      } else {
        await firestore.collection("eventos").add(evento);
      }
    } catch (err) {
      console.error(err);
    }

    onFormSuccess();
  };

  return (
    <div>
      <form onSubmit={onFormSubmit} className={classes.formDiv}>
        <TextField
          required
          className={classes.field}
          label="Nombre"
          type="text"
          variant="outlined"
          value={nombre}
          onChange={handleNombreChange}
        />
        <TextField
          required
          className={classes.field}
          label="Dirección"
          type="text"
          variant="outlined"
          value={direccion}
          onChange={handleDireccionChange}
        />
        <MuiPickersUtilsProvider utils={moment}>
          <DateTimePicker
            required
            ampm={false}
            className={classes.field}
            inputVariant={"outlined"}
            value={timestamp}
            onChange={handleDateChange}
            format={"YYYY/MM/DD - HH:mm"}
          />
        </MuiPickersUtilsProvider>
        <div className={classes.buttonBar}>
          <Button
            color="primary"
            variant="contained"
            type={"submit"}
            disabled={isSaving && isLoaded(auth)}
          >
            {buttonText}
          </Button>
          <Button
            className={classes.cancelButton}
            color="secondary"
            variant="contained"
            onClick={onFormCancel}
            disabled={isSaving}
          >
            Cancelar
          </Button>
        </div>
      </form>
    </div>
  );
};

export default EventoForm;
