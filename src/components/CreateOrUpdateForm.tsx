import React from "react";
import { FormProps } from "../types";
import { Card, CardContent, Typography } from "@material-ui/core";
import Spinner from "./Spinner";

interface Props extends FormProps {
  isCreate: boolean;
  title: string;
  formComponent: React.FC<FormProps>;
}
const CreateOrUpdateForm: React.FC<Props> = ({
  isCreate,
  formComponent,
  title,
  entity,
  ...rest
}) => {
  const FormComponent = formComponent;
  const children = isCreate ? (
    <FormComponent {...rest} />
  ) : entity ? (
    <FormComponent entity={entity} {...rest} />
  ) : (
    <Spinner/>
  );

  const cardTitle = isCreate ? "Crear" : "Actualizar";

  return (
    <Card>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {`${cardTitle} ${title}`}
        </Typography>
        {children}
      </CardContent>
    </Card>
  );
};

export default CreateOrUpdateForm;
