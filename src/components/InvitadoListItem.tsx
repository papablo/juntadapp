import React from "react";
import Invitado from "../models/Invitado";
import { Card, CardContent } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

interface Props {
  invitado: Invitado;
}

const useStyles = makeStyles({
  card: { marginTop: "1em", marginBottom: "1em" }
});

const InvitadoListItem: React.FC<Props> = ({ invitado }) => {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardContent>{invitado.email}</CardContent>
    </Card>
  );
};
export default InvitadoListItem;
