import React from "react";
import { Card, CardHeader, CardContent } from "@material-ui/core";
import { indexableEntity } from "../types";

interface Props {
  entity: indexableEntity;
  title?: string;
  labels: { label: string; name: string }[];
}

const DetailCard: React.FC<Props> = ({ entity, title, labels }) => {
  const titleSection = <CardHeader>{title}</CardHeader>;
  return (
    <div>
      <Card raised>
        {title && titleSection}
        <CardContent>
          {labels.map(({ label, name }, i) => (
            <div key={i}>
              <h4>{label}</h4>
              <p>{entity[name]}</p>
            </div>
          ))}
        </CardContent>
      </Card>
    </div>
  );
};

export default DetailCard;
