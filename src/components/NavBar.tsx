import React, { useState } from "react";
import AppBar from "@material-ui/core/AppBar";
import {
  Toolbar,
  Typography,
  makeStyles,
  createStyles,
  IconButton,
  Menu,
  MenuItem
} from "@material-ui/core";

import UserIcon from "@material-ui/icons/AccountCircle";
import { useFirebase, isLoaded } from "react-redux-firebase";
import { useSelector } from "react-redux";
import { AuthQuery } from "../types";

const useStyles = makeStyles(
  createStyles({
    root: {
      display: "flex"
    },
    title: {
      flexGrow: 1
    }
  })
);

const NavBar: React.FC = () => {
  const classes = useStyles();

  const [anchorMenu, setAnchorMenu] = useState<null | HTMLElement>(null);

  const isMenuOpen = Boolean(anchorMenu);

  const auth = useSelector(({ firebase: { auth } }: AuthQuery) => {
    return auth;
  });
  const firebase = useFirebase();

  const handleUserIconClick = ({
    currentTarget
  }: React.MouseEvent<HTMLElement>) => {
    setAnchorMenu(currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorMenu(null);
  };

  const handleLogoutClick = async () => {
    await firebase.logout();
  };

  return (
    <div>
      <AppBar position="sticky">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Juntadapp
          </Typography>
          {/* Acá iría el botón para login */}
          <Typography>
            {isLoaded(auth) && `Hola, ${auth.displayName}`}
          </Typography>
          <IconButton onClick={handleUserIconClick} color={"inherit"}>
            <UserIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Menu
        anchorEl={anchorMenu}
        id={"user-menu"}
        keepMounted
        transformOrigin={{ vertical: "center", horizontal: "left" }}
        open={isMenuOpen}
        onClose={handleMenuClose}
      >
        <MenuItem onClick={handleLogoutClick}>Logout</MenuItem>
      </Menu>
    </div>
  );
};

export default NavBar;
