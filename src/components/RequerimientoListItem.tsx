import React, { useState, ChangeEvent, useEffect } from "react";
import IRequerimiento from "../models/Requerimiento";
import {
  Card,
  CardContent,
  IconButton,
  TextField,
  makeStyles,
  createStyles
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import DeleteIcon from "@material-ui/icons/Delete";
import { RequerimientoManipulation, AuthQuery } from "../types";
import ConfirmationDialog from "./ConfirmationDialog";
import { useFirestore, isLoaded } from "react-redux-firebase";
import IEvento from "../models/Evento";
import { useSelector } from "react-redux";

interface Props extends RequerimientoManipulation {
  requerimiento: IRequerimiento;
  evento: IEvento;
}

const useStyles = makeStyles(
  createStyles({
    rootDiv: { marginBottom: "1em" },
    content: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "baseline",
      flexWrap: "wrap"
    },
    textField: {
      marginLeft: "1em",
      marginRight: "1em",
      width: "5em"
    },
    deleteButton: {
      marginLeft: "3em"
    }
  })
);

const RequerimientoListItem: React.FC<Props> = ({
  evento,
  requerimiento,
  quantityCallback
}) => {
  const classes = useStyles();

  const [cantidad, setCantidad] = useState(requerimiento.cantidad);
  const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);

  const auth = useSelector(({ firebase: { auth } }: AuthQuery) => auth);

  useEffect(() => {
    setCantidad(requerimiento.cantidad);
  });

  const onClickAdd = () => {
    if (cantidad < requerimiento.necesitado) {
      setCantidad(prevstate => {
        const newState = prevstate + 1;

        quantityCallback(requerimiento.id, newState);

        return newState;
      });
    }
  };

  const onClickMinus = () => {
    if (cantidad > 0) {
      setCantidad(prevState => {
        const newState = prevState - 1;

        quantityCallback(requerimiento.id, newState);

        return newState;
      });
    }
  };

  const handleCantidadChange = ({
    target: { value }
  }: ChangeEvent<HTMLInputElement>) => {
    if (cantidad > 0 && cantidad < requerimiento.necesitado) {
      setCantidad(() => {
        let _value = parseInt(value);

        if (!_value) {
          _value = 0;
        }

        quantityCallback(requerimiento.id, _value);

        return _value;
      });
    }
  };

  const handleNegativeClick = () => {
    setDeleteDialogOpen(false);
  };

  const firestore = useFirestore();

  const handlePositiveClick = async () => {
    try {
      firestore.delete(
        `eventos/${evento.id}/requerimientos/${requerimiento.id}`
      );
    } catch (error) {
      console.error(error);
    }
  };

  const handleDelete = () => {
    setDeleteDialogOpen(true);
  };

  return (
    <div className={classes.rootDiv}>
      <Card raised>
        <CardContent className={classes.content}>
          <div>
            <h4>{requerimiento.articulo}</h4>
          </div>
          <div>
            <IconButton color={"primary"} onClick={onClickAdd}>
              <AddIcon />
            </IconButton>
            <TextField
              className={classes.textField}
              value={cantidad}
              type={"number"}
              onChange={handleCantidadChange}
            />
            <IconButton color={"secondary"} onClick={onClickMinus}>
              <RemoveIcon />
            </IconButton>
            de {requerimiento.necesitado}
            {isLoaded(auth) && auth.email === evento.anfitrion && (
              <IconButton
                onClick={handleDelete}
                className={classes.deleteButton}
                color={"secondary"}
              >
                <DeleteIcon />
              </IconButton>
            )}
          </div>
        </CardContent>
      </Card>

      {deleteDialogOpen && (
        <ConfirmationDialog
          open={deleteDialogOpen}
          title={"Confirmar Borrado?"}
          affirmativeAnswer={"Sí"}
          negativeAnswer={"No"}
          onClickNegative={handleNegativeClick}
          onClickPositive={handlePositiveClick}
          content={"Confirmar Borrado"}
        />
      )}
    </div>
  );
};
export default RequerimientoListItem;
