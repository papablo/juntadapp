import React, { useState } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button
} from "@material-ui/core";

interface Props {
  open: boolean;
  title: string;
  content: string;
  affirmativeAnswer: string;
  negativeAnswer: string;
  onClickNegative: { (): void };
  onClickPositive: { (): void };
}

const ConfirmationDialog: React.FC<Props> = ({
  open,
  title,
  content,
  affirmativeAnswer,
  negativeAnswer,
  onClickNegative,
  onClickPositive
}) => {
  const [openDialog, setOpenDialog] = useState(open);

  const handleClose = () => {
    setOpenDialog(false);
    onClickNegative();
  };

  const handlePositive = () => {
    handleClose();
    onClickPositive();
  };

  const handleNegative = () => {
    handleClose();
  };

  return (
    <div>
      <Dialog
        open={openDialog}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {content}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleNegative} color="primary">
            {negativeAnswer}
          </Button>
          <Button onClick={handlePositive} color="primary" autoFocus>
            {affirmativeAnswer}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ConfirmationDialog;
