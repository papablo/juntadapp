import React, { useState, ChangeEvent } from "react";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  DialogContentText,
  TextField,
  DialogActions,
  Button
} from "@material-ui/core";
import { useFirestore } from "react-redux-firebase";
import IEvento from "../models/Evento";

interface Props {
  open: boolean;
  onClose: { (): void };
  evento: IEvento;
}

const VotacionFormDialog: React.FC<Props> = ({ open, onClose, evento }) => {
  const [isSaving, setIsSaving] = useState(false);

  const [dialogOpen, setDialogOpen] = useState(open);
  const [asunto, setAsunto] = useState("");

  const handleAsuntoChange = ({
    target: { value }
  }: ChangeEvent<HTMLInputElement>) => {
    setAsunto(value);
  };

  const handleClose = () => {
    setDialogOpen(false);
    onClose();
  };

  const firestore = useFirestore();

  const handleCreate = async () => {
    setIsSaving(true);

    const votacion = {
      asunto,
      votos: 0
    };

    try {
      await firestore.add(`eventos/${evento.id}/votaciones`, votacion);
    } catch (error) {
      console.error(error);
    }

    handleClose();
  };

  return (
    <div>
      <Dialog
        fullWidth
        open={dialogOpen}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Crear Votación</DialogTitle>
        <DialogContent>
          <DialogContentText>¿Qué asunto se debe decidir?</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            value={asunto}
            onChange={handleAsuntoChange}
            id="asunto-input"
            label="Asunto"
            type="text"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button disabled={isSaving} onClick={handleClose} color="primary">
            Cancelar
          </Button>
          <Button disabled={isSaving} onClick={handleCreate} color="primary">
            Crear Votación
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default VotacionFormDialog;
