import React from "react";
import { CircularProgress } from "@material-ui/core";
import { makeStyles, createStyles } from "@material-ui/styles";

const useStyle = makeStyles(
  createStyles({
    root: {
      display: "flex",
      justifyContent: "center"
    }
  })
);

export default function Spinner() {
  const classes = useStyle();
  return (
    <div className={classes.root}>
      <CircularProgress style={{ height: "8em", width: "8em" }} />
    </div>
  );
}
