import React from "react";
import Invitado from "../models/Invitado";
import InvitadoListItem from "./InvitadoListItem";

interface Props {
  invitados: Invitado[];
}

const InvitadoList: React.FC<Props> = ({ invitados }) => {
  return (
    <div>
      {invitados.map((invitado: Invitado, index: number) => {
        return <InvitadoListItem key={index} invitado={invitado} />;
      })}
    </div>
  );
};
export default InvitadoList;
