import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
  RouteComponentProps
} from "react-router-dom";

import NavBar from "./components/NavBar";
import { routes, RoutePath } from "./routes";
import { makeStyles, createStyles } from "@material-ui/core/styles";
import {
  Container,
  Card,
  CardContent,
  CardHeader,
  Typography
} from "@material-ui/core";
import { useFirebase, isLoaded, isEmpty } from "react-redux-firebase";
import { useSelector } from "react-redux";
import { AuthQuery } from "./types";
import GoogleButton from "react-google-button";
import Spinner from "./components/Spinner";

const useStyles = makeStyles(
  createStyles({
    root: {
      paddingTop: "1em"
    }
  })
);

const PrivateRoute: React.FC<RoutePath> = ({ exact, component, path }) => {
  const auth = useSelector(({ firebase: { auth } }: AuthQuery) => {
    return auth;
  });

  const classes = useStyles();

  if (isLoaded(auth)) {
    return (
      <Route
        path={path}
        exact={exact}
        render={routeProps => {
          const C: React.FC<RouteComponentProps> = component;
          return !isEmpty(auth) ? (
            <div>
              <NavBar />
              <Container className={classes.root}>
                <C {...routeProps} />
              </Container>
            </div>
          ) : (
            <Redirect to={"/login"} />
          );
        }}
      />
    );
  } else {
    return <Spinner />;
  }
};

const loginStyles = makeStyles({
  root: {
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  loginCardContent: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  instructionsText: {
    marginBottom: "2em"
  }
});

const LoginPage: React.FC = () => {
  const firebase = useFirebase();

  const classes = loginStyles();

  const auth = useSelector(({ firebase: { auth } }: AuthQuery) => {
    return auth;
  });

  return (
    <div className={classes.root}>
      <Card raised>
        <CardContent className={classes.loginCardContent}>
          <CardHeader title={"Ingrese a Juntadapp"} />

          <Typography className={classes.instructionsText}>
            Sólo con poseer una cuenta de Google ya puede disfrutar de Juntadapp
          </Typography>
          {isLoaded(auth) && !isEmpty(auth) ? (
            <Redirect to={"/"} />
          ) : (
            <GoogleButton
              onClick={() => {
                firebase.login({
                  provider: "google",
                  type: "popup"
                });
              }}
            />
          )}
        </CardContent>
      </Card>
    </div>
  );
};

const App: React.FC = () => {
  return (
    <div>
      <Router>
        <Switch>
          <Route path="/login" component={LoginPage} />
          {routes.map((route: RoutePath, index: number) => {
            return <PrivateRoute key={index} {...route} />;
          })}
        </Switch>
      </Router>
    </div>
  );
};

export default App;
